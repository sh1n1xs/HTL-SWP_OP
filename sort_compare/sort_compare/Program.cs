﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace sort_compare
{
    class Program
    {
        static void Main(string[] args)
        {
            //user input vars
            int cycles = 5;
            int sortArrayLengh = 100;


            bool cont = true;
            do
            {
                try
                {
                    Console.WriteLine("input compare repeate cycles:");
                    cycles = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("input the sort array lengh:");
                    sortArrayLengh = Convert.ToInt32(Console.ReadLine());
                    cont = true;
                }
                catch
                {
                    cont = false;
                    Console.WriteLine("error pleas retry...");
                }

            } while (!cont);


            // -------------

            int[] unsortedArray;
            SortCount[,] results = new SortCount[cycles, 5];
            TimeSpan[,] timeTake = new TimeSpan[cycles, 5];
            DateTime tmp;

            //cycles
            for (int i = 0; i <= (cycles - 1); i++)
            {
                unsortedArray = generateUnsortedArray(sortArrayLengh);

                tmp = DateTime.Now;
                results[i, 0] = bubbleSort(ref unsortedArray);
                timeTake[i, 0] = DateTime.Now - tmp;
                unsortedArray = generateUnsortedArray(sortArrayLengh);

                tmp = DateTime.Now;
                results[i, 1] = insertSort(ref unsortedArray);
                timeTake[i, 1] = DateTime.Now - tmp;

                unsortedArray = generateUnsortedArray(sortArrayLengh);

                tmp = DateTime.Now;
                results[i, 2] = selectionSort(ref unsortedArray);
                timeTake[i, 2] = DateTime.Now - tmp;

                unsortedArray = generateUnsortedArray(sortArrayLengh);

                tmp = DateTime.Now;
                results[i, 3] = stableSelectionSort(ref unsortedArray);
                timeTake[i, 3] = DateTime.Now - tmp;

                unsortedArray = generateUnsortedArray(sortArrayLengh);
                
                tmp = DateTime.Now;
                QuickSort(ref unsortedArray, 0, unsortedArray.Length - 1);
                results[i, 4] = qsCounter;
                timeTake[i, 4] = DateTime.Now - tmp;

            }

            //calc average
            int[] avg_comp = new[] {0, 0, 0, 0, 0};
            int[] avg_swap = new[] {0, 0, 0, 0, 0};
            double[] avg_time = new[] {0.0, 0.0, 0.0, 0.0, 0.0};

            for (int i = 0; i <= (cycles - 1); i++)
            {
                for (int f = 0; f <= 4; f++)
                {
                    avg_comp[f] = avg_comp[f] + results[i, f].compare;
                    avg_swap[f] = avg_swap[f] + results[i, f].swap;
                    avg_time[f] = avg_time[f] + timeTake[i, f].TotalMilliseconds;
                }
            }


            //outputs data in a not ideal way
            Console.WriteLine("Data for bubble Sort:");
            Console.WriteLine("avg comp: " + avg_comp[0]);
            Console.WriteLine("avg swap: " + avg_swap[0]);
            Console.WriteLine("avg time: " + avg_time[0]);

            Console.WriteLine("Data for insert Sort:");
            Console.WriteLine("avg comp: " + avg_comp[1]);
            Console.WriteLine("avg swap: " + avg_swap[1]);
            Console.WriteLine("avg time: " + avg_time[1]);

            Console.WriteLine("Data for selection Sort:");
            Console.WriteLine("avg comp: " + avg_comp[2]);
            Console.WriteLine("avg swap: " + avg_swap[2]);
            Console.WriteLine("avg time: " + avg_time[2]);

            Console.WriteLine("Data for unstable selection Sort:");
            Console.WriteLine("avg comp: " + avg_comp[3]);
            Console.WriteLine("avg swap: " + avg_swap[3]);
            Console.WriteLine("avg time: " + avg_time[3]);

            Console.WriteLine("Data for quick Sort:");
            Console.WriteLine("avg comp: " + avg_comp[4]);
            Console.WriteLine("avg swap: " + avg_swap[4]);
            Console.WriteLine("avg time: " + avg_time[4]);

            Console.ReadKey();
        }

        static int[] generateUnsortedArray(int n)
        {
            int[] arr = new int[n];
            Random rnd = new Random();

            foreach (var i in arr)
            {
                arr[i] = rnd.Next(0, int.MaxValue / 500);
            }

            return arr;
        }

        static SortCount insertSort(ref int[] uarr)
        {
            SortCount count = new SortCount();
            count.compare = 0;
            count.swap = 0;
            int[] arr = uarr;
            int n = arr.Length;
            for (int i = 1; i < n; ++i)
            {
                int key = arr[i];
                int j = i - 1;
                while (j >= 0 && arr[j] > key)
                {
                    arr[j + 1] = arr[j];
                    j = j - 1;
                    count.compare++;
                }

                arr[j + 1] = key;
                count.swap++;
            }

            return count;
        }

        static SortCount bubbleSort(ref int[] uarr)
        {
            SortCount count = new SortCount();
            count.compare = 0;
            count.swap = 0;
            int[] arr = uarr;
            int n = arr.Length;
            for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (arr[j] > arr[j + 1])
                {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                    count.swap++;
                }

            count.compare++;

            return count;
        }

        static SortCount selectionSort(ref int[] uarr)
        {
            SortCount count = new SortCount();
            count.compare = 0;
            count.swap = 0;
            int[] arr = uarr;
            int n = arr.Length;
            for (int i = 0; i < n - 1; i++)
            {
                int min_idx = i;
                for (int j = i + 1; j < n; j++)
                {
                    if (arr[j] < arr[min_idx])
                    {
                        min_idx = j;
                    }

                    count.compare++;
                }

                int temp = arr[min_idx];
                arr[min_idx] = arr[i];
                arr[i] = temp;
                count.swap++;
            }

            return count;
        }

        static SortCount stableSelectionSort(ref int[] arr)
        {
            SortCount count = new SortCount();
            count.compare = 0;
            count.swap = 0;
            int[] a = arr;
            int n = a.Length;
            for (int i = 0; i < n - 1; i++)
            {
                int min = i;
                for (int j = i + 1; j < n; j++)
                {
                    if (a[min] > a[j])
                    {
                        min = j;
                    }

                    count.compare++;
                }

                int key = a[min];
                while (min > i)
                {
                    a[min] = a[min - 1];
                    min--;
                    count.swap++;
                }


                a[i] = key;
            }

            return count;
        }

        static SortCount qsCounter = new SortCount();

        static int QuickSortPartition(int[] array, int low, int high)
        {
            //1. Select a pivot point.
            int pivot = array[high];

            int lowIndex = (low - 1);

            //2. Reorder the collection.
            for (int j = low; j < high; j++)
            {
                qsCounter.compare++;
                if (array[j] <= pivot)
                {
                    lowIndex++;
                    qsCounter.swap++;
                    int temp = array[lowIndex];
                    array[lowIndex] = array[j];
                    array[j] = temp;
                }
            }

            int temp1 = array[lowIndex + 1];
            array[lowIndex + 1] = array[high];
            array[high] = temp1;

            return lowIndex + 1;
        }

        static void doQuickSort(int[] array, int low, int high)
        {
            if (low < high)
            {
                int partitionIndex = QuickSortPartition(array, low, high);

                //3. Recursively continue sorting the array
                doQuickSort(array, low, partitionIndex - 1);
                doQuickSort(array, partitionIndex + 1, high);
            }

        }

        static void QuickSort(ref int[] array, int low, int high)
        {
            qsCounter = new SortCount();
            qsCounter.swap = 0;
            qsCounter.compare = 0;
            doQuickSort(array, low, high);
        }
    }

    struct SortCount
    {
        public int compare;
        public int swap;
    }
}