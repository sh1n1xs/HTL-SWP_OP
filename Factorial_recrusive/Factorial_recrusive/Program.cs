﻿using System;

namespace Factorial_recrusive
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a number (integer only)");
            int inputNumber = 0;
            bool input = false;
            do
            {
                input = false;
                try
                {
                    inputNumber = Convert.ToInt32(Console.ReadLine());

                }
                catch (Exception e)
                {
                    input = true;
                    Console.WriteLine("Enter a number (integer only)");
                }
            } while (input);
            
            Console.Write("Output:");
            Console.WriteLine(CalcFactorial(inputNumber));
            Console.ReadKey();
        }


        public static int CalcFactorial(int num)
        {
            if ((num - 1) == 0)
            {
                return num;
            }
            return CalcFactorial(num-1) * num;
        }
    }
}