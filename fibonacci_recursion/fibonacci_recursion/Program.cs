﻿using System;
using System.Net;

namespace fibonacci_recursion
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 0;
            Console.WriteLine("insert number pls");
            try
            {
                num = Convert.ToInt32(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("wrong imput");
                Environment.Exit(1);
            }

            DateTime start, end;
            TimeSpan timetaken;
            Console.WriteLine("recrusive:");
            start = DateTime.Now;
            Console.WriteLine(fibonacci_recursion(num));
            end = DateTime.Now;
            timetaken = end - start;
            Console.WriteLine("Time: " + timetaken.TotalMilliseconds);
            Console.WriteLine("tail recrusive:");
            start = DateTime.Now;
            Console.WriteLine(fibonacci_tailrecursion(num));
            end = DateTime.Now;
            timetaken = end - start;
            Console.WriteLine("Time: " + timetaken.TotalMilliseconds);
        }


        static int fibonacci_recursion(int n)
        {
            if (n == 0)
            {
                return 0;
            }
            if(n == 1 || n == 2)
            {
                return 1;
            }
            return fibonacci_recursion(n - 1) + fibonacci_recursion(n - 2);
        }


        static int fibonacci_tailrecursion(int n)
        {
            if (n == 0)
            {
                return 0;
            }

            return fibonacci_tailrecursion(0,1, n - 1);
        }

        static int fibonacci_tailrecursion(int prev, int cur, int n)
        {
            if (n == 0)
            {
                return cur;
            }

            return fibonacci_tailrecursion(cur, prev + cur, n-1);
        }
    }
}