﻿using System;

namespace BinarySearch
{
    public class arraySearch
    {
        public int[] SearchArray
        {
            get { return _searchArray; }
        }

        private int[] _searchArray;

        public arraySearch(int arrayLengh, bool randomNumbers)
        {
            if (arrayLengh <= 0)
            {
                arrayLengh = 10;
            }
            _searchArray = new int[arrayLengh];
            
            Random rnd = new Random();
            for (int i = 0; i < this._searchArray.Length; i++)
            {
                if (randomNumbers == false)
                {
                    this._searchArray[i] = i + 1;
                }
                else
                {
                    this._searchArray[i] = rnd.Next(0, int.MaxValue);
                    System.Threading.Thread.Sleep(1); //Sleep for 1 milisec because c# randomizer
                }
            }

            if (randomNumbers)
            {
                Array.Sort(this._searchArray); //sorts array yay
            }
        }

        public bool SequentialSearch(int searchedInt)
        {
            bool wasFound = false;

            for (int i = 0; i < this._searchArray.Length; i++)
            {
                if (this._searchArray[i] == searchedInt)
                {
                    wasFound = true;
                }
            }

            return wasFound;
        }

        public bool BinarySearch(int searchedInt)
        {
            return bns(searchedInt, 0, this._searchArray.Length-1);
        }

        private bool bns(int searchedInt, int startPos, int endPos)
        {
            
            int check = Convert.ToInt32(Math.Round((((Convert.ToDouble(endPos) - Convert.ToDouble(startPos)) / 2.0) + Convert.ToDouble(startPos)), MidpointRounding.AwayFromZero));
            if (check == startPos) //we ran out of array ==> it doesn't exist
            {
                return false;
            }

            if (this._searchArray[check] == searchedInt) //the number was found
            {
                return true;
            }

            int newStart = 0, newEnd = 0;
            if (searchedInt > this._searchArray[check])
            {
                newEnd = endPos;
                newStart = check;
            }

            if (searchedInt < this._searchArray[check])
            {
                newEnd = check;
                newStart = startPos;
            }

            return bns(searchedInt, newStart, newEnd);
            
        }

    }
}