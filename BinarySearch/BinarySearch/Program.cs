﻿using System;

namespace BinarySearch
{
    class Program
    {
        static void Main(string[] args)
        {
            int counter = 10;
            Console.WriteLine("Search performance test");
            Console.WriteLine(LineParagraph());
            Console.WriteLine(LineParagraph());
            for (int i = 1; i <= 6; i++)
            {
                Console.WriteLine();
                PerfTest(Convert.ToInt32(Math.Pow(Convert.ToDouble(counter), Convert.ToDouble(i))));
                Console.WriteLine();
                Console.WriteLine(LineParagraph());
            }
            
        }

        static void PerfTest(int arrLength)
        {
            DateTime startTime;
            DateTime endTime;
            TimeSpan timeDuration;
            Console.WriteLine();
            Console.WriteLine("Test with an array length of " + arrLength + ":");
            arraySearch ars = new arraySearch(arrLength, false);
            int searchInt = Convert.ToInt32(Convert.ToDouble(arrLength) / 2.5);
            Console.Write("Sequential search: ");
            startTime = DateTime.Now;
            ars.SequentialSearch(searchInt);
            endTime = DateTime.Now;
            timeDuration = endTime.Subtract(startTime);
            Console.Write(timeDuration.Duration() + Environment.NewLine);
            Console.Write("Binary search: ");
            startTime = DateTime.Now;
            ars.BinarySearch(searchInt);
            endTime = DateTime.Now;
            timeDuration = endTime.Subtract(startTime);
            Console.Write(timeDuration.Duration() + Environment.NewLine);

        }
        static string LineParagraph() // for the looks
        {
            string p = "";
            for (int i = 0; i < Console.WindowWidth; i++)
            {
                p = p + "#";
            }

            return p;
        }
    }
}