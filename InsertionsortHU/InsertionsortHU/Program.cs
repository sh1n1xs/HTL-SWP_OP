﻿using System;

namespace InsertionsortHU
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] sortThis = new[] {50, 60, 80, 10, 11, 23, 45, 78, 19, 50, 56, 54, 71, 150, 687, 408, 165, 106, 709, 504};
            TimeSpan insertASC, insertDESC, bubble;
            DateTime start, end;
            
            start = DateTime.UtcNow;
            int[] sortedASC = insertationsortASC(sortThis);
            end =  DateTime.UtcNow;
            insertASC = end - start;
            
            start = DateTime.UtcNow;
            int[] sortedDESC = insertationsortDESC(sortThis);
            end =  DateTime.UtcNow;
            insertDESC = end - start;
            
            start = DateTime.UtcNow;
            int[] sortedBubble = bublesort(sortThis);
            end =  DateTime.UtcNow;
            bubble = end - start;

            Console.WriteLine(Environment.NewLine +"ASC:");
            for (int i = 0; i <= sortedASC.Length-1; i++)
            {
                Console.WriteLine(sortedASC[i]);
            }
            Console.WriteLine("time taken: " + insertASC.Ticks);
            Console.WriteLine(Environment.NewLine +"DESC:");
            for (int i = 0; i <= sortedDESC.Length-1; i++)
            {
                Console.WriteLine(sortedDESC[i]);
            }
            Console.WriteLine("time taken: " + insertDESC.Ticks);
            Console.WriteLine(Environment.NewLine +"bubblesort:");
            for (int i = 0; i <= sortedBubble.Length-1; i++)
            {
                Console.WriteLine(sortedBubble[i]);
            }
            Console.WriteLine("time taken: " + bubble.Ticks);
        }

        static int[] insertationsortASC(int[] ar)
        {
            int[] tmp = ar;
            for (int i = 1; i < tmp.Length; i++)
            {
                int x = tmp[i];
                int j = Math.Abs(Array.BinarySearch(tmp, 0, i, x) + 1);
                Array.Copy(tmp, j, ar, j + 1, i - j);
                tmp[j] = x;
            }

            return tmp;
        }

        static int[] insertationsortDESC(int[] ar)
        {
            int[] tmp = insertationsortASC(ar); //pfusch
            int[] descTMP = new int[tmp.Length];
            for (int i = 0; i < tmp.Length; i++)
            {
                descTMP[(tmp.Length - 1) - i] = tmp[i];
            }

            return descTMP;
        }
        
        
        static int[] bublesort(int[] ar)
        {
            int[] nar = new int[ar.Length];
            nar = ar;
            int tmp;
            int noSort = 0;
            for (int i = 0; i <= nar.Length - 2; i++)
            {
                if (ar[i] > ar[i + 1])
                {
                    tmp = ar[i + 1]; //swaps vars
                    ar[i + 1] = ar[i];
                    ar[i] = tmp;
                }
                else
                {
                    noSort++;
                }
            }

            if (noSort == ar.Length - 1)
            {
                return nar;
            }
            return bublesort(nar);
        }
        
    }
}