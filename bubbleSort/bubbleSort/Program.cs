﻿using System;

namespace bubbleSort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] sortThisPls = new[] {6, 5, 3, 1, 8, 7, 2, 4};
            Console.WriteLine("unsorted array = ");
            foreach (int i in sortThisPls)
            {
                Console.WriteLine(i);
            }
            Console.WriteLine("----------------..");
            Console.WriteLine("");
            DateTime start = DateTime.Now;
            sortThisPls = bublesort(sortThisPls);
            DateTime end = DateTime.Now;
            Console.WriteLine("sorted array = ");
            foreach (int i in sortThisPls)
            {
                Console.WriteLine(i);
            }
            Console.WriteLine("");
            TimeSpan timeTaken = end - start;
            Console.WriteLine("time taken (in ms): " + timeTaken.Milliseconds);

        }

        static int[] bublesort(int[] ar)
        {
            int[] nar = new int[ar.Length];
            nar = ar;
            int tmp;
            int noSort = 0;
            for (int i = 0; i <= nar.Length - 2; i++)
            {
                if (ar[i] > ar[i + 1])
                {
                    tmp = ar[i + 1]; //swaps vars
                    ar[i + 1] = ar[i];
                    ar[i] = tmp;
                }
                else
                {
                    noSort++;
                }
            }

            if (noSort == ar.Length - 1)
            {
                return nar;
            }
            return bublesort(nar);
        }
    }
}